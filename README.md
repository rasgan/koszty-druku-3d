Koszty wydruku 3D
=================

Arkusz kalkulacyjny XLS pomagający określić przybliżone koszty przy wydrukach 3D.

Wersja 0.1
----------
Obliczanie kosztów prądu i materiału z uwzględnieniem strat materiału i prądu rozruchu drukarki